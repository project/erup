<?php

/**
 * @file
 * ERUP module shows a warning when referencing unpublished content.
 */

/**
 * Implements hook_menu().
 */
function erup_menu() {
  $items['admin/config/content/erup'] = array(
    'title' => 'Unpublished reference warnings',
    'description' => 'Show a warning when referencing unpublished content.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('erup_admin_form'),
    'access arguments' => array('administer erup'),
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function erup_permission() {
  $permissions = array(
    'administer erup' => array(
      'title' => t('Administer Entity Reference Unpublished Warnings'),
    ),
    'show erup warnings' => array(
      'title' => t('Show Entity Reference Unpublished Warnings'),
    ),
  );
  return $permissions;
}

/**
 * ERUP config form.
 *
 * @param array $form
 *
 * @return array
 *  The configuration form.
 */
function erup_admin_form(array $form) {
  // For which node types do we activate this stuff?
  $types = node_type_get_types();
  $form['erup_intro'] = array(
    '#markup' => t('Show unpublished warnings on:'),
  );
  if (is_array($types)) {
    foreach ($types as $type) {
      $form['erup_' . $type->type] = array(
        '#type' => 'checkbox',
        '#default_value' => variable_get('erup_' . $type->type, TRUE),
        '#title' => $type->name,
      );
    }
  }
  return system_settings_form($form);
}

/**
 * Implements hook_help().
 */
function erup_help($path, $arg) {
  switch ($path) {
    case 'admin/config/content/erup':
      $output = '<p>' . t('When viewing content that has an entity reference field, and that field references unpublished content, then show a warning. <br />Warnings are only shown for users with the <em>Show Entity Reference Unpublished Warnings</em> permission.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_node_view().
 */
function erup_node_view(stdClass $node, $view_mode, $langcode) {
  if ($view_mode != 'full' || !variable_get('erup_' . $node->type, TRUE) || !user_access('show erup warnings')) {
    return;
  }
  $wrapper = entity_metadata_wrapper('node', $node);
  $field_listing = field_info_instances('node', $node->type);
  foreach ($field_listing as $field_name => $field_instance_info) {
    $field_base_info = field_info_field($field_name);
    if ($field_base_info['type'] != 'entityreference') {
      continue;
    }
    // Good ol' Drupal 7 — the structure is different if cardinality is 1, vs.
    // more. This is fixed in D8.
    if ($field_base_info['cardinality'] == 1) {
      if (is_object($wrapper->{$field_name}->value())) {
        _erup_check_node($wrapper->{$field_name}->value(), $field_instance_info['label']);
      }
    }
    else {
      foreach ($wrapper->{$field_name}->value() as $node) {
        if (is_object($node)) {
          _erup_check_node($node, $field_instance_info['label']);
        }
      }
    }
  }
}

/**
 * If the given node is unpublished, show the warning message about the given
 * field.
 *
 * @param stdClass $node
 * @param string $field_label
 */
function _erup_check_node(stdClass $node, $field_label) {
  if (isset($node->status) && $node->status != NODE_PUBLISHED) {
    if (node_access('update', $node)) {
      $path = "node/$node->nid/edit";
    }
    elseif (node_access('view', $node)) {
      $path = "node/$node->nid";
    }
    else {
      drupal_set_message(t('The %label field references the unpublished content %title. You may need to publish that content first. But you do not have the necessary permissions.', array('%label' => $field_label, '%title' => $node->title)), 'warning', FALSE);
      return;
    }
    drupal_set_message(t('The %label field references the unpublished content !title. You may need to publish that content first.', array('%label' => $field_label, '!title' => l($node->title, $path))), 'warning', FALSE);
  }
}
